# rust-muldiv [![crates.io](https://img.shields.io/crates/v/muldiv.svg)](https://crates.io/crates/muldiv) [![Build Status](https://travis-ci.org/sdroege/rust-muldiv.svg?branch=master)](https://travis-ci.org/sdroege/rust-muldiv) [![docs.rs](https://docs.rs/muldiv/badge.svg)](https://docs.rs/muldiv)

Provides a trait for numeric types to perform combined multiplication and
division with overflow protection.

## LICENSE

rust-muldiv is licensed under the MIT license ([LICENSE](LICENSE) or
http://opensource.org/licenses/MIT).

## Contribution

Any kinds of contributions are welcome as a pull request.

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in send-cell by you shall be licensed under the MIT license as above,
without any additional terms or conditions.
